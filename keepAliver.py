'''
Created on Aug 11, 2016

@author: sangmin
'''


import xmlrpclib
import time
import g2sLogger
import sys
import os
import settings
# from supervisor import xmlrpc
import logging
import datetime

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")

#import models

try:
    import django.utils.importlib
except ImportError:
    from importlib import import_module

if django.VERSION[1] > 5:
    django.setup()

from django.db import connection

class Main():

    def __init__(self):
        self.logger = logging.getLogger('keepAliver')
        # self.dbSession = self.connectDb()
        self.supervisorRtc = self.connectSupervisor()
        self.lastRestartProcessTime = datetime.datetime.now()
        # self.cursor = None
        self.logger.info('### Initialization completion ###')

    def __getLastRestartProcessTime(self):
        if self.lastRestartProcessTime and datetime.datetime.now() - self.lastRestartProcessTime > datetime.timedelta(minutes=settings.RESTART_WAITING_TIME):
            self.lastRestartProcessTime = None
            return True

        return False

    # def __getCursor(self):

    #     if not self.cursor:
    #         if not self.dbSession:
    #             self.dbSession = self.connectDb()
    #         self.cursor = self.dbSession.cursor()

    #     return self.cursor

    # def connectDb(self):

    #     dbconn = None
    #     db_host = settings.GET_DB_HOST()
    #     try:
    #         dbconn = cx_Oracle.connect(db_host)

    #     except cx_Oracle.OperationalError:
    #         self.logger.exception("oracle open operational error")
    #         dbconn = None
    #     except cx_Oracle.DatabaseError:
    #         self.logger.exception("oracle open database error")
    #         dbconn = None
    #     except cx_Oracle.InterfaceError:
    #         self.logger.exception("oracle open interface error")
    #     except:
    #         self.logger.exception('ETC db connection error')
    #         dbconn = None
    #     return dbconn

    def connectSupervisor(self):
        supervisorServ = xmlrpclib.Server('http://localhost:{}/RPC2'.format(settings.MEGA_PORT))
        #supervisorServ = xmlrpclib.ServerProxy(
        #    'http://localhost:{}'.format(settings.MEGA_HOST),
        #    transport=xmlrpc.SupervisorTransport(
        #        None, None, 'unix://{}/temp/supervisor.sock'.format(settings.MEGA_LOCATION)
        #    )
        #)

        return supervisorServ

    def restart_process(self):
        self.logger.info('Start restart Mega Process')
        stopFlag = True
        startFlag = True
        while stopFlag:
            time.sleep(1)
            self.logger.critical('Try stop mega process')
            result = self.supervisorRtc.supervisor.stopAllProcesses()
            # self.logger.critical('supervisor stop process success : {}'.format(result))

            if result == [] or result[0]['status'] == 80:
                self.logger.critical('supervisor stop process success : {}'.format(result))
                stopFlag = False
            else:
                self.logger.critical('supervisor stop process fail : {}'.format(result))

        while startFlag:
            time.sleep(1)
            self.logger.critical('Try start mega process')
            result = self.supervisorRtc.supervisor.startAllProcesses()
            # self.logger.critical('supervisor start process success : {}'.format(result))

            if result == [] or result[0]['status'] == 80:
                self.logger.critical('supervisor start process success : {}'.format(result))
                startFlag = False
            else:
                self.logger.critical('supervisor start process fail : {}'.format(result))

        self.lastRestartProcessTime = datetime.datetime.now()
        self.logger.critical('restart mega Process complete')

    # def query(self, sql):
    #     result = []
    #     try:
    #         cur = self.__getCursor()
    #         cur.execute(sql)
    #         result = [row for row in cur]
    #     except:
    #         self.logger.exception('DB query error: query is {}'.format(sql))

    #     return result

    # def do_update(self, resultList):
    #     # rtnValue = []
    #     sql = """\
    #         update g2s.G2S_TRANSPORTSTATE_LOG set \
    #             check_yn = 'Y' where id = {}
    #     """
    #     for result in resultList:
    #         if not self.query(sql.format(result)):
    #             return False
    #     return True

    def do_check(self, property):

        '''
        when comms not receive ack 30times insert data row

        1. check property database table
        1-1. when restart flag is True, ignore and update check_yn. not restart mega_manager
        2. if exist row,  restart mega manager
        3. Turn on restart flag. (It is maintained only for five minutes.))

        return update row count
        '''

        sql = """UPDATE G2S_MEGA.G2S_TRANSPORTSTATE_LOG@g2s_{} \
            SET CHECK_YN = 'Y', LST_UPD_USER_ID = 'aliver', LST_UPD_TIME = sysdate \
            WHERE CHECK_YN = 'N' AND RECORD_ST = 'A' AND state = '0'
        """.format(property)

        self.logger.debug('update query sql : {}'.format(sql))
        with connection.cursor() as c:
            c.execute(sql)

            result = c.rowcount
        self.logger.debug('## update result : {}'.format(result))

        return int(result)

    def main_loop(self):
        self.logger.info('### Start main loop ###')
        while True:
            try:
                # self.dbSession.ping() # every 60 second send db ping
                time.sleep(settings.LOOP_INTERVAL)
                updateCnt = 0
                for site in settings.SITES:
                    updateCnt += self.do_check(site)
                    if updateCnt > 0:
                        self.logger.info('## {} site update Cnt is {}'.format(site, updateCnt))
                        if self.__getLastRestartProcessTime():
                            self.logger.critical('Call restart mega process function({})'.format(updateCnt))
                            self.restart_process()
                            #self.lastRestartProcessTime = datetime.datetime.now()
                        else:
                            self.logger.critical('## Not yet ready for restart mega manager too short')
                            self.logger.critical('## last restart time is {}'.format(self.lastRestartProcessTime))
                        # self.do_update(result)

            except django.db.DatabaseError:

                # Kill process group(App, Msg Process), Supervisor start the
                # processes automatically.

                self.logger.exception('############## DataBase Error. keepAliver Exit #############')
                self.logger.critical(
                    "########## Kill process group %d ##########" % os.getpgid(
                        os.getpid()
                    )
                )
                os.killpg(os.getpgid(os.getpid()), 9)
                break

            except KeyboardInterrupt:
                self.logger.warning('keyboard interrrupt program exit')
                break

            except:
                self.logger.exception('Main Loop Error!!!!')


if __name__ == '__main__':
    g2sLogger.G2sLogger(settings.LOG_LEVEL, True, name="keepAliver")

    try:
        keepAliveInterval = int(sys.argv[2])     #seconds
    except IndexError:
        keepAliveInterval = 60

    m = Main()
    m.main_loop()

